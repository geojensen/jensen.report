const loginModal$ = $('#loginModal'),
    loginModalCloseBtn$ = $('#loginModalCloseBtn'),
    loginForm$ = $('#loginForm'),
    loginFormSuccess$ = $('#loginFormSuccess');

$(function () {
    loginModal$.on('shown.bs.modal', function (e) {
        $('#main-content').addClass('blurred');
    });

    loginModal$.on('hidden.bs.modal', function (e) {
        $('#main-content').removeClass('blurred');
    });

    loginForm$.on('submit', function (e) {
        e.preventDefault();

        $.post({
            url: $(this).prop('action'),
            data: $(this).serializeArray(),
            success: function () {
                loginFormSuccess$.removeAttr('hidden');
            }
        });
    })
});
$(function () {
    // Retrieve your data from localStorage
    let saveData = localStorage.getItem('vim.cards.user') || null;

    if (!saveData) {
        $('.nav-link').text('Login');
        $('.nav-link').on('click', function () {
            loginModal$.show();
            loginModalCloseBtn$.removeAttr('hidden');
        });
    } else {
        saveData = JSON.parse(saveData);
        $('.nav-link').text('Hi, ' + saveData.name).removeAttr('data-target').removeAttr('data-toggle');
    }
});
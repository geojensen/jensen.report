const loginModal$ = $('#loginModal'),
    loginModalCloseBtn$ = $('#loginModalCloseBtn'),
    loginForm$ = $('#loginForm'),
    loginFormSuccess$ = $('#loginFormSuccess');

$(function () {
    loginModal$.on('shown.bs.modal', function (e) {
        $('#main-content').addClass('blurred');
    });

    loginModal$.on('hidden.bs.modal', function (e) {
        $('#main-content').removeClass('blurred');
    });

    loginForm$.on('submit', function (e) {
        e.preventDefault();

        $.post({
            url: $(this).prop('action'),
            data: $(this).serializeArray(),
            success: function () {
                loginFormSuccess$.removeAttr('hidden');
            }
        });
    })
});

$(function () {
    // Retrieve your data from localStorage
    let saveData = localStorage.getItem('vim.cards.user') || null;

    if (!saveData) {
        $('.nav-link').text('Login');
        $('.nav-link').on('click', function () {
            loginModal$.show();
            loginModalCloseBtn$.removeAttr('hidden');
        });
    } else {
        saveData = JSON.parse(saveData);
        $('.nav-link').text('Hi, ' + saveData.name).removeAttr('data-target').removeAttr('data-toggle');
    }
});

$(function () {
    let params = getParams($('#article-entry'));

    if (params.user) {
        localStorage.setItem('vim.cards.user', JSON.stringify(params.user));
    }

    if (params.url) {
        console.log('URL ' + params.url);
        window.history.pushState(params.user, "Vim.cards", '/' + params.url);
    }

    // Retrieve your data from localStorage
    let saveData = localStorage.getItem('vim.cards.user') || null;

    if (!saveData) {
        setTimeout(function () {
            loginModal$.modal('show');
        }, 2000);
    } else {
        saveData = JSON.parse(saveData);
        $('.nav-link').text('Hi, ' + saveData.name);
    }
});

function getParams(script) {
    return {
        loginModal: script.attr('data-login-modal'),
        loginModalCloseBtn: script.attr('data-login-close-btn'),
        loginForm: script.attr('data-login-form'),
        loginFormSuccessMsg: script.attr('data-login-form-success-msg'),

        user: script.attr('data-user') ? JSON.parse(script.attr('data-user')) : null,
        url: script.attr('data-url')
    }
}
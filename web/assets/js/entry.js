$(function () {
    let params = getParams($('#article-entry'));

    if (params.user) {
        localStorage.setItem('vim.cards.user', JSON.stringify(params.user));
    }

    if (params.url) {
        console.log('URL ' + params.url);
        window.history.pushState(params.user, "Vim.cards", '/' + params.url);
    }

    // Retrieve your data from localStorage
    let saveData = localStorage.getItem('vim.cards.user') || null;

    if (!saveData) {
        setTimeout(function () {
            loginModal$.modal('show');
        }, 2000);
    } else {
        saveData = JSON.parse(saveData);
        $('.nav-link').text('Hi, ' + saveData.name);
    }
});

function getParams(script) {
    return {
        user: script.attr('data-user') ? JSON.parse(script.attr('data-user')) : null,
        url: script.attr('data-url')
    }
}